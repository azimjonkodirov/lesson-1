# Create simple HTML webpage where you have to write:
## Task list
a.	Head title ( Linear colored ) <br>
b.	Description ( Simple text ) <br>
c.	Rectangle figure ( color red ) <br>
## Output
<img src="./image.png" alt="Kitten"
	title="A cute kitten" width="100%" height="100%" />

## License
[MIT](https://choosealicense.com/licenses/mit/)

<!-- This is my contribution - Madiyor Abdukhashimov -->